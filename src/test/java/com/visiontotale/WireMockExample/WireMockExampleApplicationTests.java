package com.visiontotale.WireMockExample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.visiontotale.WireMockExample.model.dto.Employee;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.json.JSONException;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import java.net.URI;
import java.net.URISyntaxException;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
class WireMockExampleApplicationTests {
	private static final int PORT = 8000;
	private static final String HOST = "localhost";
	private static int id = 3;
	private static WireMockServer wireMockServer;

	@BeforeAll
	public static void setup() {
		wireMockServer = new WireMockServer(PORT);

		// Allow the server to start before the test starts
		wireMockServer.start();

		// Configure the server to intercept all the requests which are coming at: http://localhost:8000
		WireMock.configureFor(HOST, PORT);
	}

	@AfterEach
	void afterEach() {
		wireMockServer.resetAll();
	}

	@Test
	@DisplayName("Get all the Employees")
	void getAllEmployeesTest() throws URISyntaxException {
		// Given
		WireMock.stubFor(WireMock
				.get("/employee/all")
				.willReturn(new ResponseDefinitionBuilder()
						.withStatus(200)  // Here, I choose to specify the status as a mock response. It's also possible to specify the body or the header.
				)
		);

		RestAssured.given()
				.when()
				.get(new URI("http://localhost:8000/employee/all"))
				.then()
				.assertThat()
				.statusCode(200);
	}

	@Test
	@DisplayName("Get employee by id")
	void getAEmployeeByIdTest() throws URISyntaxException {
		// Given
		WireMock.stubFor(WireMock
				.get("/employee/find/" + id)
				.willReturn(new ResponseDefinitionBuilder()
						.withStatus(200)
						.withBodyFile("json/employe_by_id.json")
				)
		);

		RestAssured.given()
				.when()
				.get(new URI("http://localhost:8000/employee/find/" + id))
				.then()
				.assertThat()
				.statusCode(200)
				.defaultParser(Parser.JSON)
				.body("id", equalTo(3))
				.body("name", equalTo("BAUER Jack"))
				.body("email", equalTo("jack.beauer@fbi.org"))
				.body("jobTitle", equalTo("CTU and Federal Agent"))
				.body("phone", equalTo("+16875554116"))
				.body("imageUrl", equalTo("url"))
				.body("employeeCode", equalTo("a9a2373f-6bb1-4988-8033-ae5e80bde4ae"));
	}

	@Test
	@DisplayName("Add a new Employee")
	void addEmployeeTest() throws URISyntaxException, JSONException, JsonProcessingException {
		//When
		Employee employee = Employee.builder()
				.name("DESLER Michelle")
				.email("desler.michelle@fbi.org")
				.jobTitle("CTU Agent")
				.phone("+16811154116")
				.imageUrl("url")
				.build();

		WireMock.stubFor(WireMock
				.post("/employee/add")
				.withHeader("Content-Type", containing("application/json"))
				.willReturn(new ResponseDefinitionBuilder()
						.withBodyFile("json/employee_returned_after_add.json")
						.withStatus(200)
				)
		);

		RestAssured.given()
				.contentType(ContentType.JSON)
				.body(employee)
				.when()
				.post(new URI("http://localhost:8000/employee/add"))
				.then()
				.assertThat()
				.statusCode(200)
				.defaultParser(Parser.JSON)
				.body("id", equalTo(10))
				.body("employeeCode", equalTo("a9a1173f-6bb1-4988-8033-ae1180bde4ae"));

	}

	@AfterAll
	public static void tearDown() {
		if(null != wireMockServer && wireMockServer.isRunning()) {
			wireMockServer.shutdown();
		}
	}
}
