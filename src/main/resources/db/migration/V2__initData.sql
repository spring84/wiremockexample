insert into employee_entity (id, name, email, job_title, phone, image_url, employee_code) values
(1, 'KENFACK Arnaud', 'arnaud.kenfack@ctu.org', 'CTU Agent', '+15764580986764', 'URL', '2a$10$u7GDdfn5vfJ.iN0BahTvoOr-iN0BahTvoOr'),
(2, 'BEATRIC Fotem', 'beatric.fotem@fbi.org', 'FBI Agent', '+15764585586764', 'URL', '2a$10$u7GDdfn5vfJ.iN0BahTvoO000Lkkl-iN0BahTvoO000Lkkl'),
(3, 'MARINA Navabi', 'navabi.marina@mosaad.org', 'MOSAAD Agent', '+15084585586333', 'URL', '2a$101234Ddfn5vfJ.iN0BahTvoO660L-iN0BahTvoO660L'),
(4, 'BAUER Jack', 'jack.bauer@ctu.org', 'CTU Agent', '+15084585580033', 'URL', 'DF$101234Ddfn5vfJ.iN0BahTvoO6604-iN0BahTvoO6604'),
(5, 'ALMEIDA Tonny', 'tonny.almeida@ctu.org', 'CTU Agent', '+15084512345633', 'URL', 'DF$101234D1235vfJ.iN-iNhytr4576khgt98756'),
(6, 'WOLKER Renee', 'renee.wolker@fbi.org', 'FBI Agent', '+16084512345773', 'URL', 'DF$101234D12377fJ.iN077-GFTR4567BHF34hgrtuygb'),
(7, 'PETROVISH Serguei', 'serguei.petrovish@fsb.org', 'FSB Agent', '+10004512340003', 'URL', '00$101230002377fJ.iN-iNjgo34put03j0959rojghj');