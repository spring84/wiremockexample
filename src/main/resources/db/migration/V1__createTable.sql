create TABLE employee_entity (
       id SERIAL PRIMARY KEY,
       name VARCHAR(100),
       email VARCHAR(50),
       job_title VARCHAR(50),
       phone VARCHAR(15),
       image_url VARCHAR(100),
       employee_code VARCHAR(200)
);