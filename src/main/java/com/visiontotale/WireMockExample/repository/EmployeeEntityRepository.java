package com.visiontotale.WireMockExample.repository;

import com.visiontotale.WireMockExample.model.entities.EmployeeEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * @author Arnaud
 * 13/10/2022
 *
 */

public interface EmployeeEntityRepository extends ReactiveCrudRepository<EmployeeEntity, Long> {
}
