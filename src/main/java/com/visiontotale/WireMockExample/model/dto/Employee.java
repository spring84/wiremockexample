package com.visiontotale.WireMockExample.model.dto;

import lombok.*;

/**
 * @author Arnaud
 * 13/10/2022
 *
 */

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private Long id;
    private String name;
    private String email;
    private String jobTitle;
    private String phone;
    private String imageUrl;
    private String employeeCode;
}
