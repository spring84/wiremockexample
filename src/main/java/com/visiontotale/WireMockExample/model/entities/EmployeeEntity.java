package com.visiontotale.WireMockExample.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * @author Arnaud
 * 13/10/2022
 *
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
public class EmployeeEntity {
    @Id
    private Long id;
    @Column()
    private String name;
    @Column()
    private String email;
    @Column()
    private String jobTitle;
    @Column()
    private String phone;
    @Column()
    private String imageUrl;
    @Column()
    private String employeeCode;
}
