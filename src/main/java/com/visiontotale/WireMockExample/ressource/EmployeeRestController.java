package com.visiontotale.WireMockExample.ressource;

import com.visiontotale.WireMockExample.model.dto.Employee;
import com.visiontotale.WireMockExample.service.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Arnaud 13/10/2022
 */
@Slf4j
@RestController
@RequestMapping("/employee")
@Tag(name = "Employee Ressource")
public class EmployeeRestController {

  private final EmployeeService employeeService;

  @Autowired
  public EmployeeRestController(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  @GetMapping("/all")
  @Operation(summary = "Get all the employees")
  public ResponseEntity<Flux<Employee>> getAllEmployees() {
    Flux<Employee> employees = employeeService.findAllEmployees();
    return new ResponseEntity<>(employees, HttpStatus.OK);
  }

  @GetMapping("/find/{id}")
  @Operation(summary = "Get an employee by id")
  public ResponseEntity<Mono<Employee>> getEmployeeById(@PathVariable("id") Long id) {
    Mono<Employee> employee = employeeService.findEmployeeById(id);
    return new ResponseEntity<>(employee, HttpStatus.OK);
  }

  @PostMapping("/add")
  @Operation(summary = "Add an employee")
  public ResponseEntity<Mono<Employee>> addEmployee(@RequestBody Employee employee) {
    Mono<Employee> newEmployee = employeeService.addEmployee(employee);
    return new ResponseEntity<>(newEmployee, HttpStatus.CREATED);
  }

  @PutMapping("/update/{id}")
  @Operation(summary = "Update an employee")
  public ResponseEntity<Mono<Employee>> updateEmployee(
      @RequestBody Employee employee, @PathVariable("id") Long id) {
    Mono<Employee> updatedEmployee = employeeService.updateEmployee(employee, id);
    return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
  }

  @DeleteMapping("/delete/{id}")
  @Operation(summary = "Delete an employee")
  public ResponseEntity<Mono<Employee>> deleteEmployee(@PathVariable("id") Long id) {
    Mono<Employee> employeeToBeDeleted = employeeService.deleteEmployee(id);
    return new ResponseEntity<>(employeeToBeDeleted, HttpStatus.OK);
  }
}
