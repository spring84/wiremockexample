package com.visiontotale.WireMockExample.config;

import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.stereotype.Component;

/**
 * @author Arnaud
 * 13/10/2022
 *
 */

@Component
public class SwaggerConfig {

    public GroupedOpenApi publicApi() {
        String[] paths = { "/**" };
        String[] packages = {"com.visiontotale.WireMockExample"};
        return GroupedOpenApi.builder()
                .group("Employee")
                .pathsToMatch(paths)
                .packagesToScan(packages)
                .addOpenApiCustomiser(openApi -> {
                    openApi.info(new Info()
                            .title("Employee APIs")
                            .description("Exposing Employees Operations")
                            .version("1.0")
                    );
                })
                .build();
    }
}
