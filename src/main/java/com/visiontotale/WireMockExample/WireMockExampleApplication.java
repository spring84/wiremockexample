package com.visiontotale.WireMockExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WireMockExampleApplication {

  public static void main(String[] args) {
    SpringApplication.run(WireMockExampleApplication.class, args);
  }
}
