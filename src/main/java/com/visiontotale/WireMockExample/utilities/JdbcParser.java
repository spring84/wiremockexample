package com.visiontotale.WireMockExample.utilities;

import java.net.URI;

/**
 * @author Arnaud
 * 13/10/2022
 *
 */

public class JdbcParser {
    public static String getJdbcHost(String jdbc) {
        return getUri(jdbc).getHost();
    }

    public static int getJdbcPort(String jdbc) {
        return getUri(jdbc).getPort();
    }

    public static String getDbName(String jdbc) {
        return getUri(jdbc).getPath().substring(1);
    }

    private static URI getUri(String jdbc) {
        return URI.create(jdbc.substring(5));
    }
}
