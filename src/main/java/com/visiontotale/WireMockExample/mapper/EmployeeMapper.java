package com.visiontotale.WireMockExample.mapper;

import com.visiontotale.WireMockExample.model.dto.Employee;
import com.visiontotale.WireMockExample.model.entities.EmployeeEntity;
import org.springframework.stereotype.Component;

/**
 * @author Arnaud
 * 13/10/2022
 *
 */

@Component
public class EmployeeMapper {
    public Employee entityToDto(EmployeeEntity employeeEntity) {
        if ( employeeEntity == null ) {
            return null;
        } else {
            Employee employee = new Employee();
            employee.setId(employeeEntity.getId());
            employee.setName(employeeEntity.getName());
            employee.setEmail(employeeEntity.getEmail());
            employee.setJobTitle(employeeEntity.getJobTitle());
            employee.setPhone(employeeEntity.getPhone());
            employee.setImageUrl(employeeEntity.getImageUrl());
            employee.setEmployeeCode(employeeEntity.getEmployeeCode());

            return employee;
        }
    }

    public EmployeeEntity dtoToEntity(Employee employee) {
        if ( employee == null ) {
            return null;
        } else {
            EmployeeEntity employeeEntity = new EmployeeEntity();
            employeeEntity.setId(employee.getId());
            employeeEntity.setName(employee.getName());
            employeeEntity.setEmail(employee.getEmail());
            employeeEntity.setJobTitle(employee.getJobTitle());
            employeeEntity.setPhone(employee.getPhone());
            employeeEntity.setImageUrl(employee.getImageUrl());
            employeeEntity.setEmployeeCode(employee.getEmployeeCode());

            return employeeEntity;
        }
    }
}
