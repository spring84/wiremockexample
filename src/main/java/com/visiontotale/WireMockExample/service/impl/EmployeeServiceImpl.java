package com.visiontotale.WireMockExample.service.impl;

import com.visiontotale.WireMockExample.mapper.EmployeeMapper;
import com.visiontotale.WireMockExample.model.dto.Employee;
import com.visiontotale.WireMockExample.repository.EmployeeEntityRepository;
import com.visiontotale.WireMockExample.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * @author Arnaud 14/10/2022
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

  private final EmployeeEntityRepository employeeEntityRepository;
  private final EmployeeMapper employeeMapper;

  @Autowired
  public EmployeeServiceImpl(
      final EmployeeEntityRepository employeeEntityRepository,
      final EmployeeMapper employeeMapper) {
    this.employeeEntityRepository = employeeEntityRepository;
    this.employeeMapper = employeeMapper;
  }

  @Override
  public Mono<Employee> addEmployee(Employee employee) {
    employee.setEmployeeCode(UUID.randomUUID().toString());
    return employeeEntityRepository
        .save(employeeMapper.dtoToEntity(employee))
        .map(employeeMapper::entityToDto);
  }

  @Override
  public Flux<Employee> findAllEmployees() {
    return employeeEntityRepository.findAll().map(employeeMapper::entityToDto);
  }

  @Override
  public Mono<Employee> updateEmployee(Employee employee, Long id) {
    /*return employeeEntityRepository
    .findById(id)
    .flatMap(foundEmployeeEntity -> {
        foundEmployeeEntity.setName(employeeMapper.dtoToEntity(employee).getName());
        foundEmployeeEntity.setEmail(employeeMapper.dtoToEntity(employee).getEmail());
        foundEmployeeEntity.setJobTitle(employeeMapper.dtoToEntity(employee).getJobTitle());
        foundEmployeeEntity.setPhone(employeeMapper.dtoToEntity(employee).getPhone());
        foundEmployeeEntity.setImageUrl(employeeMapper.dtoToEntity(employee).getImageUrl());
        foundEmployeeEntity.setEmployeeCode(employeeMapper.dtoToEntity(employee).getEmployeeCode());
        return employeeEntityRepository.save(foundEmployeeEntity);
    })
    .map(employeeMapper::entityToDto);*/

    return employeeEntityRepository
        .findById(id)
        .map(
            (foundEmployeeEntity) -> {
              foundEmployeeEntity.setName(employeeMapper.dtoToEntity(employee).getName());
              foundEmployeeEntity.setEmail(employeeMapper.dtoToEntity(employee).getEmail());
              foundEmployeeEntity.setJobTitle(employeeMapper.dtoToEntity(employee).getJobTitle());
              foundEmployeeEntity.setPhone(employeeMapper.dtoToEntity(employee).getPhone());
              foundEmployeeEntity.setImageUrl(employeeMapper.dtoToEntity(employee).getImageUrl());

              return foundEmployeeEntity;
            })
        .flatMap(employeeEntityRepository::save)
        .map(employeeMapper::entityToDto);
  }

  @Override
  public Mono<Employee> deleteEmployee(Long id) {
    return findEmployeeById(id)
        .flatMap(
            employeeToBedeleted ->
                employeeEntityRepository
                    .delete(employeeMapper.dtoToEntity(employeeToBedeleted))
                    .then(Mono.just(employeeToBedeleted)));
  }

  @Override
  public Mono<Employee> findEmployeeById(Long id) {
    return employeeEntityRepository.findById(id).map(employeeMapper::entityToDto);

    /*return employeeEntityRepository
    .findById(id)
    .map((foundEmployeeEntity) -> employeeMapper.entityToDto(foundEmployeeEntity));*/
  }
}
