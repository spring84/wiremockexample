package com.visiontotale.WireMockExample.service;

import com.visiontotale.WireMockExample.model.dto.Employee;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Arnaud 13/10/2022
 */
public interface EmployeeService {
  Mono<Employee> addEmployee(Employee employee);

  Flux<Employee> findAllEmployees();

  Mono<Employee> updateEmployee(Employee employee, Long id);

  Mono<Employee> deleteEmployee(Long id);

  Mono<Employee> findEmployeeById(Long id);
}
