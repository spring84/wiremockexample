# WireMockExample

WireMock is a library that does a real good job in stubbing and mocking web services. 
This example shows how to use it to test rest APIs.

References: 
- https://blog.nebrass.fr/playing-with-reactive-spring-boot/
- https://www.swtestacademy.com/wiremock-junit-5-rest-assured/
- https://www.baeldung.com/rest-assured-tutorial